package ab2.impl.Dippold;

import java.util.Set;

import ab2.Factory;
import ab2.Mealy;
import ab2.PDA;

public class FactoryImpl implements Factory {

    @Override
    public Mealy getEmptyMealy() {
	return new MealyImpl();
    }

    @Override
    public PDA getEmptyPDA() {
	return new PDAImpl();
    }

    @Override
    public PDA getPDAFromCFG(char startSymbol, Set<String> rules) {
	return null;
    }
}
