package ab2.impl.Dippold;

import ab2.Factory;
import ab2.PDA;

import java.util.EmptyStackException;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class PDAImpl implements PDA {
    private int numStates;
    private int initialState;
    private boolean numStatesInitialized = false;
    private Set<Integer> acceptingStates;
    private Set<Character> inputChars;
    private Set<Character> stackChars;
    private Set<PDATransition> transitions = new HashSet<>();

    /**
     * Setzt die Anzahl an Zuständen des Automaten
     *
     * @param numStates
     * @throws IllegalArgumentException wenn numStates <= 0
     */
    @Override
    public void setNumStates(int numStates) throws IllegalArgumentException {
        if(numStates <= 0) throw new IllegalArgumentException();
        this.numStates = numStates;
        this.numStatesInitialized = true;
    }

    /**
     * Setzt den Startzustand des Automaten
     *
     * @param initialState
     * @throws IllegalArgumentException falls numStates < 0 oder >= numStates
     * @throws IllegalStateException    falls numStates noch nicht gesetzt wurde
     */
    @Override
    public void setInitialState(int initialState) throws IllegalArgumentException, IllegalStateException {
        if(numStatesInitialized == false) throw new IllegalStateException();
        if(numStates < 0 || initialState >= numStates || initialState < 0) throw new IllegalArgumentException();

        this.initialState = initialState;
    }

    /**
     * Setzt die akzeptierenden Zustände
     *
     * @param acceptingStates
     * @throws IllegalArgumentException falls ein Zustand < 0 oder >= numStates ist
     * @throws IllegalStateException    falls numStates noch nicht gesetzt wurde
     */
    @Override
    public void setAcceptingState(Set<Integer> acceptingStates) throws IllegalArgumentException, IllegalStateException {
        if(numStatesInitialized == false) throw new IllegalStateException();
        for(Integer i : acceptingStates){
            if(i < 0 || i >= numStates) throw new IllegalArgumentException();
        }
        this.acceptingStates = acceptingStates;
    }

    /**
     * Setzt die Menge an erlaubten Zeichen der Eingabe
     *
     * @param chars
     */
    @Override
    public void setInputChars(Set<Character> chars) {
        this.inputChars = chars;
    }

    /**
     * Setzt die Menge an erlaubten Zeichen des Stacks
     *
     * @param chars
     */
    @Override
    public void setStackChars(Set<Character> chars) {
        this.stackChars = chars;
    }

    /**
     * Fügt eine Transiation hinzu
     *
     * @param fromState
     * @param charReadTape
     * @param charReadStack
     * @param charWriteStack
     * @param toState
     * @throws IllegalArgumentException falls ein Zustand nicht valide ist, die Zeichen nicht erlaubt sind
     * @throws IllegalStateException    falls numStates, inputChars oder stackChars nicht gesetzt wurden
     */
    @Override
    public void addTransition(int fromState, Character charReadTape, Character charReadStack, Character charWriteStack, int toState) throws IllegalArgumentException, IllegalStateException {
        if(numStatesInitialized == false) throw new IllegalStateException();
        if(inputChars == null) throw new IllegalStateException();
        if(stackChars == null) throw new IllegalStateException();

        if(fromState < 0 || fromState >= numStates) throw new IllegalArgumentException();
        if(toState <0 || toState >= numStates) throw new IllegalArgumentException();
        if(!inputChars.contains(charReadTape)) throw new IllegalArgumentException();
        if(!(stackChars.contains(charReadStack) || charReadStack == null)) throw new IllegalArgumentException();
        if(!(stackChars.contains(charWriteStack) || charWriteStack == null)) throw new IllegalArgumentException();

        transitions.add(new PDATransition(fromState, charReadTape, charReadStack, charWriteStack, toState));
    }

    /**
     * Prüft, ob eine Eingbe von dem PDA akzeptiert wird (dh Teil der Sprache des
     * PDA ist)
     *
     * @param input
     * @throws IllegalArgumentException der Input aus nicht erlaubten Zeichen besteht
     * @throws IllegalStateException    falls numStates, inputChars oder stackChars nicht gesetzt wurden
     */
    @Override
    public boolean accepts(String input) throws IllegalArgumentException, IllegalStateException {
        if(!numStatesInitialized || inputChars == null || stackChars == null) throw new IllegalStateException();
        for(char c : input.toCharArray()){
            if(!inputChars.contains(c)) throw new IllegalArgumentException();
        }

        int currentState = initialState;
        Stack<Character> stack = new Stack<>();
        boolean t = acceptsRecursive(input, stack, currentState);
        return t;
    }

    private boolean acceptsRecursive(String input, Stack<Character> stack, int currentState){
        if(input.isEmpty()) return(acceptingStates.contains(currentState) && stack.empty());
        char in = input.charAt(0);

        Character topOfStack;
        topOfStack = stack.empty() ? null : stack.peek();
        Set<PDATransition> transitions = findTransitions(currentState, in, topOfStack);
        if(transitions.isEmpty()) return false; //Couldn't find transition

        for(PDATransition t : transitions){
            Stack<Character> newStack = (Stack<Character>)stack.clone();
            if(t.getReadStack() != null) newStack.pop();
            if(t.getWriteStack() != null) newStack.push(t.getWriteStack());
            currentState = t.getToState();
            if(acceptsRecursive(input.substring(1), newStack, currentState)) return true;
        }

        return false;
    }

    private Set<PDATransition> findTransitions(int fromState, Character in, Character topOfStackChar){
        Set<PDATransition> transitionsFound = new HashSet<>();
        for(PDATransition t : transitions){
            if(t.getFromState() == fromState && t.getReadTape() == in && (t.getReadStack() == topOfStackChar || t.getReadStack() == null)) transitionsFound.add(t);
        }
        return transitionsFound;
    }

    /**
     * Erzeugt einen neuen PDA, indem an den PDA (this) der überegebene PDA
     * angehängt wird, sodass die aktzeptierte Sprache des zurückgegebenen PDAs der
     * Konkatenation der Sprachen der beiden PDAs entspricht. Keiner der beiden PDAs
     * darf verändert werden. es muss ein neuer PDA erzeugt werden.
     *
     * @param pda
     * @throws IllegalArgumentException falls numStates, inputChars oder stackChars des übergebenen PDA nicht gesetzt wurden
     * @throws IllegalStateException    falls numStates, inputChars oder stackChars nicht gesetzt wurden
     */
    @Override
    public PDA append(PDA pda) throws IllegalArgumentException, IllegalStateException {
        PDAImpl myPda = (PDAImpl)pda;
        if(!numStatesInitialized || inputChars == null || stackChars == null) throw new IllegalStateException();
        if(!myPda.getNumStatesInitialized() || myPda.getInputChars() == null ||  myPda.getStackChars() == null) throw new IllegalArgumentException();

        Set<PDATransition> myTransitions = myPda.getTransitions();
        int myInitialState = myPda.getInitialState();
        Set<Integer> myAcceptingStates = myPda.getAcceptingStates();

        PDAImpl newPDA = new PDAImpl();
        newPDA.setNumStates(this.getNumStates() + myPda.getNumStates());
        newPDA.setInitialState(this.getInitialState());

        Set<Integer> newAcceptingStates = new HashSet<>();
        for(int i : myPda.getAcceptingStates()){
            i = i+this.getNumStates();
            newAcceptingStates.add(i);
        }
        newPDA.setAcceptingState(newAcceptingStates);

        Set<Character> newInputChars = this.getInputChars();
        for(char c : myPda.getInputChars()) if(!newInputChars.contains(c)) newInputChars.add(c);
        newPDA.setInputChars(newInputChars);

        Set<Character> newStackChars = this.getStackChars();
        for(char c : myPda.getStackChars()) if(!newStackChars.contains(c)) newStackChars.add(c);
        newPDA.setStackChars(newStackChars);

        Set<PDATransition> newTransitions = new HashSet<>();
        for(PDATransition t : this.getTransitions()){
            if(this.acceptingStates.contains(t.getToState())){
                t.setToState(newPDA.getInitialState()+this.getNumStates());
            }
            newTransitions.add(t);
        }
        for(PDATransition t : newPDA.getTransitions()){
            t.setFromState(t.getFromState()+this.getNumStates());
            t.setToState(t.getToState()+this.getNumStates());
            newTransitions.add(t);
        }
        return newPDA;
    }

    private boolean getNumStatesInitialized(){
        return numStatesInitialized;
    }
    private Set<Character> getInputChars(){
        return inputChars;
    }
    private Set<Character> getStackChars(){
        return stackChars;
    }
    private Set<PDATransition> getTransitions(){
        return transitions;
    }
    private int getInitialState(){
        return initialState;
    }
    private Set<Integer> getAcceptingStates(){
        return acceptingStates;
    }
    private int getNumStates(){
        return numStates;
    }

    /**
     * Erzeugt einen neuen PDA, indem der PDA (this) und der überegebene PDA
     * vereinigt werden. Die Sprache des zurückgegebenen PDAs entspricht der
     * Vereinigung der Sprachen der beiden PDAs. Keiner der beiden PDAs darf
     * verändert werden. es muss ein neuer PDA erzeugt werden.
     *
     * @param pda
     * @throws IllegalArgumentException falls numStates, inputChars oder stackChars des übergebenen PDA nicht gesetzt wurden
     * @throws IllegalStateException    falls numStates, inputChars oder stackChars nicht gesetzt wurden
     */
    @Override
    public PDA union(PDA pda) throws IllegalArgumentException, IllegalStateException {
        return null;
    }

    /**
     * Gibt an, ob der PDA ein DPDA ist (es wird nicht überprüft ob L(PDA) Element von DCFL ist)
     *
     * @throws IllegalStateException falls numStates, inputChars oder stackChars von zumindest einem PDA nicht gesetzt wurden
     */
    @Override
    public boolean isDPDA() throws IllegalStateException {
        /**if(!numStatesInitialized || inputChars == null || stackChars == null) throw new IllegalStateException();

        for(int i=0; i<numStates; i++){
            for(PDATransition t : transitions){
                if(t.getFromState() == i)
            }
            //if(t.getFromState() == fromState && t.getReadTape() == in && (t.getReadStack() == topOfStackChar || t.getReadStack() == null)) transitionsFound.add(t);
        }*/

        return false;
    }
}
