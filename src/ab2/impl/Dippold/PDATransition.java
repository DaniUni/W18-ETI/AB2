package ab2.impl.Dippold;

import ab2.Transition;

public class PDATransition extends Transition {
    private Character readStack;

    public PDATransition(int fromState, Character readTape, Character readStack, Character writeStack, int toState) {
        super(fromState, toState, readTape, writeStack);
        this.readStack = readStack;
    }

    public Character getReadStack(){
        return readStack;
    }

    public Character getWriteStack(){
        return getCharWrite();
    }

    public Character getReadTape() {
        return getCharRead();
    }

    public void setReadStack(Character readStack) {
        this.readStack = readStack;
    }
}
