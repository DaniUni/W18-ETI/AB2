package ab2.impl.Dippold;

import ab2.Mealy;
import ab2.Transition;

import java.util.HashSet;
import java.util.Set;

public class MealyImpl implements Mealy {
    private int numStates;
    private boolean numStatesInitialized = false;
    private int initialState;
    private Set<Character> readChars;
    private Set<Character> writeChars;
    private Set<Transition> transitions;

    /**
     * Setzt die Anzahl an Zuständen des Automaten
     *
     * @param numStates
     * @throws IllegalArgumentException wenn numStates <= 0
     */
    @Override
    public void setNumStates(int numStates) throws IllegalArgumentException {
        if(numStates <= 0) throw new IllegalArgumentException();

        this.numStates = numStates;
        this.numStatesInitialized = true;
    }

    /**
     * Setzt den Startzustand des Automaten
     *
     * @param initialState
     * @throws IllegalArgumentException falls numStates < 0 oder >= numStates
     * @throws IllegalStateException    falls numStates noch nicht gesetzt wurde
     */
    @Override
    public void setInitialState(int initialState) throws IllegalArgumentException, IllegalStateException {
        if(numStatesInitialized == false) throw new IllegalStateException();

        if(numStates < 0) throw new IllegalArgumentException();
        if(initialState < 0 || initialState >= numStates) throw new IllegalArgumentException();

        this.initialState = initialState;
    }

    /**
     * Setzt die Menge an erlaubten Eingabezeichen
     *
     * @param chars
     */
    @Override
    public void setReadChars(Set<Character> chars) {
        this.readChars = chars;
    }

    /**
     * Setzt die Menge an erlaubten Ausgabezeichen
     *
     * @param chars
     */
    @Override
    public void setWriteChars(Set<Character> chars) {
        this.writeChars = chars;
    }

    /**
     * Fügt eine Transiation hinzu
     *
     * @param fromState
     * @param charRead
     * @param charWrite
     * @param toState
     * @throws IllegalArgumentException falls ein Zustand nicht valide ist, die
     *                                  Zeichen nicht erlaubt sind oder die
     *                                  Transition einen Nichtdeterminismus
     *                                  erzeugen würde
     * @throws IllegalStateException    falls numStates, readChars oder
     *                                  writeChars nicht gesetzt wurden
     */
    @Override
    public void addTransition(int fromState, char charRead, Character charWrite, int toState) throws IllegalArgumentException, IllegalStateException {
        if(fromState > numStates || toState > numStates) throw new IllegalArgumentException();
        if(!readChars.contains(charRead) || !writeChars.contains(charWrite)) throw new IllegalArgumentException();
        for(Transition t : transitions){
            if(t.getFromState() == fromState && t.getCharRead() == charRead) throw new IllegalArgumentException(); // Transition with same fromState and charRead already exists
        }

        transitions.add(new Transition(fromState, toState, charRead, charWrite));
    }

    /**
     * Wandelt den Mealy-Autoaten in einen Moore Automaten um. Die leere Ausgabe
     * (null) wird bezüglich des State splittings wie ein Zeichen behandelt. Der
     * bestehende Automat wird nicht verändert.
     *
     * @throws IllegalStateException falls numStates, readChars oder writeChars
     *                               nicht gesetzt wurden
     */
    @Override
    public Mealy toMoore() {
        if(numStatesInitialized == false || readChars == null || writeChars == null) throw new IllegalStateException();

        //First find out those states which have more than 1 outputs associated with them.
        Set<Transition> newTransitions = new HashSet<>();

        for(Transition t : transitions){
            for(Transition nt : newTransitions){
                if(t.getFromState() == nt.getFromState() && t.getCharWrite() != nt.getCharWrite()){
                    //found same state but different output -> new state
                    numStates++;
                    //newTransitions.add(new Transition(t.getFromState(), ))
                }
            }
        }

        //Create two states for these states.

        return this;
    }

    /**
     * Erzeugt die Ausgabe, die durch die Verarbeitung der Eingabe entsteht. Kann
     * die Eingabe nicht verarbeitet werden, soll null zurück gegeben werden
     *
     * @param input
     * @throws IllegalStateException falls numStates, readChars oder writeChars
     *                               nicht gesetzt wurden
     */
    @Override
    public String produced(String input) throws IllegalStateException {
        if(numStatesInitialized == false) throw new IllegalStateException();
        if(readChars == null) throw new IllegalStateException();
        if(writeChars == null) throw new IllegalStateException();

        String out = "";
        int currentState = initialState;
        for(char in : input.toCharArray()){
            Transition t = findTransition(currentState, in);
            out += t.getCharWrite();
            currentState = t.getToState();
        }
        return out;
    }

    private Transition findTransition(int fromState, Character readChar){
        for(Transition t : transitions){
            if(t.getFromState() == fromState && t.getCharRead() == readChar) return t;
        }
        return null;
    }

    /**
     * Minimalisiert den Automaten. Der Automat wird nicht verändert.
     *
     * @throws IllegalStateException falls numStates, readChars oder writeChars
     *                               nicht gesetzt wurden
     */
    @Override
    public Mealy minimize() {
        if(numStatesInitialized == false) throw new IllegalStateException();
        if(readChars == null) throw new IllegalStateException();
        if(writeChars == null) throw new IllegalStateException();


        return null;
    }

    /**
     * Liefert die Anzahl der Zustände
     *
     * @throws IllegalStateException falls numStates noch nicht gesetzt wurde
     */
    @Override
    public int getNumStates() throws IllegalStateException {
        if(numStatesInitialized == false) throw new IllegalStateException();
        return numStates;
    }

    /**
     * Liefert die Transitionen des Automaten
     *
     * @return
     */
    @Override
    public Set<Transition> getTransitions() {
        return transitions;
    }
}
